package com.appcraftz.medicallpharma.Dialogs;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.appcraftz.medicallpharma.R;
import com.squareup.picasso.Picasso;

//import static com.appcraftz.medicallpharma.R.id.imageView;

public class PrescriptionDialog extends AppCompatActivity implements View.OnClickListener{

    ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_prescription_dialogue);

        setTitle("Prescription");
        String url = getIntent().getStringExtra("url");

        Toast.makeText(getApplicationContext(),"Click on Image to exit",Toast.LENGTH_LONG).show();

        imageView = (ImageView) findViewById(R.id.iv_order_prescription);
        Picasso.with(this)
                .load(url)
                .error(R.drawable.no_image)
                .into(imageView);

        imageView.setOnClickListener(this);
    }
    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        finish();
    }
}
