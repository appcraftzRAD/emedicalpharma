package com.appcraftz.medicallpharma.ServerManager;

import android.util.Log;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by silentcoder on 20/5/17.
 */

public class EncryptManager {

    public String getMD5encryption(String password){

        try {

            MessageDigest messageDigest = java.security.MessageDigest.getInstance("MD5");
            messageDigest.update(password.getBytes());
            byte mDigest[] = messageDigest.digest();

            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < mDigest.length; i++) {
                String h = Integer.toHexString(0xFF & mDigest[i]);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();
        }catch (NoSuchAlgorithmException e){
            e.printStackTrace();
        }
        Log.e("EncryptManager","Error in Encryption");
        return "";
    }
}
