package com.appcraftz.medicallpharma.ServerManager;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;


import com.appcraftz.medicallpharma.Helpers.SPHelper;
import com.appcraftz.medicallpharma.Helpers.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by appcraftz on 07/03/17.
 */

public class ServerManager {

    private OkHttpClient httpClient;
    private Context con;
    private ProgressDialog progressDialog;
    final private String key;


    public ServerManager(Context con)
    {

        httpClient = new OkHttpClient();
        this.con = con;
        progressDialog = new ProgressDialog(con);
        key="appcraftz2016@intern";

    }


    public ServerManager()
    {

        httpClient = new OkHttpClient();
        progressDialog = new ProgressDialog(con);
        key="appcraftz2016@intern";

    }

    public boolean login(final String emailid, final String password)
    {
        class LoginTask extends AsyncTask<Object, Object, Boolean> {
            Boolean status = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog.setMessage("Logging You..");
                progressDialog.show();

            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
                progressDialog.cancel();
            }

            @Override
            protected Boolean doInBackground(Object... params) {
                String encrypted_password = new EncryptManager().getMD5encryption(password);
                try
                {
                    Log.d("ServerMgr",emailid+" "+encrypted_password);
                    RequestBody formBody = new FormBody.Builder()
                            .add("key",key)
                            .add("email_idd",emailid)
                            .add("password",password)
                            .build();

                    Request request = new Request.Builder()
                            .url("http://217.61.17.155/~appcrf/MedicineApp/pharma_app/pharmalogin.php")
                            .method("POST",formBody)
                            .build();

                    Log.e("REQUEST",request.toString());
                    Response response = httpClient.newCall(request).execute();

                    String sResponse = response.body().string();

                    Log.e("RESPONSE", sResponse.substring(6));

                    JSONObject jsonObject = new JSONObject(sResponse.substring(6));
                    String successString = jsonObject.optString("message");

                    Log.e("SrvMgr",String.valueOf(successString.compareTo("Successfully logined lol.")));
                    if(successString.compareTo("Successfully logined lol.") == 0) {
                        SPHelper.setSP(con,"email_idd",emailid);
                        SPHelper.setSP(con,"shop_name",jsonObject.optString("shop_name"));
                        SPHelper.setSP(con,"owner_id",jsonObject.optString("owner_id"));
                        SPHelper.setSP(con,"password",jsonObject.optString("password"));
                        SPHelper.setSP(con,"license_no",jsonObject.optString("license_no"));
                        SPHelper.setSP(con,"address",jsonObject.optString("address"));
                        SPHelper.setSP(con,"area",jsonObject.optString("nearest_station"));
                        SPHelper.setSP(con,"pin_code",jsonObject.optString("pin_code"));
                        SPHelper.setSP(con,"contact_no",jsonObject.optString("contact_no"));
                        SPHelper.setSP(con,"owner_name",jsonObject.optString("owner_name"));
                        status=true;
                    }
                    else
                        status=false;
                }catch (Exception ex)
                {
                    ex.printStackTrace();
                    status = false;
                }
                Log.d("CheckLogin",""+status);
                return status;
            }
        }

        LoginTask logintask = new LoginTask();
        logintask.execute();
        progressDialog.setMessage("Logging In..");
        progressDialog.setMessage("Logging In..");

        while(logintask.status == null){
        }
        progressDialog.cancel();
        return logintask.status;
    }

    //Check when webservices are ready
    public Boolean register(final String sownername, final String scontactnum, final String slicencenum, final String sshopname,
                            final String semailid, final String saddress, final String sarea, final String spincode,
                            final String spassword, final String stoken) {
        class registerTask extends AsyncTask<Object, Object, Boolean> {
            final String TAG = "RegisterTask";
            Boolean status = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog.setMessage("Registering you...");
                progressDialog.show();
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
                progressDialog.cancel();
            }

            @Override
            protected Boolean doInBackground(Object... params) {
                String encrypted_password = new EncryptManager().getMD5encryption(spassword);
                try
                {
                    RequestBody formBody = new FormBody.Builder()
                            .add("key",key)
                            .add("owner_name",sownername)
                            .add("shop_name",sshopname)
                            .add("email_idd",semailid)
                            .add("password",spassword)
                            .add("license_no",slicencenum)
                            .add("address",saddress)
                            .add("nearest_station",sarea)
                            .add("pin_code",spincode)
                            .add("contact_no",scontactnum)
                            .add("token_no",stoken)
                            .build();

                    Request request = new Request.Builder()
                            .url("http://217.61.17.155/~appcrf/MedicineApp/pharma_app/register_pharma.php")
                            .method("POST",formBody)
                            .build();

                    Response response = httpClient.newCall(request).execute();

                    String sResponse = response.body().string();

                    Log.d(TAG, sResponse);
                    sResponse =getProperObject(sResponse);
                    JSONObject jsonObject = new JSONObject(sResponse);
                    String successString = jsonObject.optString("message");

                    if(successString.compareTo("welcome to medicall") == 0) {
                        status=true;
                    }
                    else
                        status=false;
                }catch (Exception ex)
                {
                    ex.printStackTrace();
                    status = false;
                }
                Log.d(TAG,""+status);
                return status;
            }
        }
        registerTask registerTask = new registerTask();
        registerTask.execute();
        while(registerTask.status == null){
        }
        return registerTask.status;
    }

    public String notifyUser(final String order_id,final String shop_name, final JSONArray medicines){
        class NotifyUser extends AsyncTask<Object, Object, Void> {
            String successString = null;
            String acceptedOrders =null;
            @Override
            protected Void doInBackground(Object... params) {
                try {
                    RequestBody formBody = new FormBody.Builder()
                            .add("key",key)
                            .add("order_id",String.valueOf(order_id))
                            .add("shop_name",shop_name)
                            .add("medicines",medicines.toString())
                            .add("contact_no",SPHelper.getSP(con,"contact_no"))
                            .add("owner_id",SPHelper.getSP(con,"owner_id"))
                            .build();

                    Log.d("Server Manager",formBody.toString());
                    Request request = new Request.Builder()
                            .url("http://217.61.17.155/~appcrf/MedicineApp/pharma_app/sushil_accept_reject_order.php")
                            .method("POST",formBody)
                            .build();
                    Response response = httpClient.newCall(request).execute();
                    String sResponse = response.body().string();
                    Log.d("Server Manager",request.toString() + "\n"+ response + "\n" +sResponse);
                    sResponse = getProperObject(sResponse);
                    JSONObject jsonObject = new JSONObject(sResponse);
                    successString = jsonObject.optString("message");
                    Log.d("Server Manager",jsonObject.optString("med"));
                    acceptedOrders = jsonObject.optString("accepted");
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                progressDialog.dismiss();
            }
        }
        NotifyUser nUser = new NotifyUser();
        nUser.execute();
        //progressDialog.setMessage("Trying...");
        //progressDialog.show();
        while(nUser.acceptedOrders == null){}
        //progressDialog.cancel();
        if(nUser.successString.compareTo("success") == 0){
            Log.d("SERVER_MANAGER",nUser.successString);
            Log.d("SERVER_MANAGER",nUser.acceptedOrders);
        }else{
            Toast.makeText(con,nUser.successString,Toast.LENGTH_LONG).show();
            return "failed";
        }
        return nUser.acceptedOrders;
        //return "failed";
    }

    private String getProperObject(String resp) {
        int i=0;
        for(i=0;i<resp.length();i++){
            if(resp.charAt(i) == '{')
                return resp.substring(i);
        }
        return null;
    }


    public boolean updateToken(){
        boolean status = false;
        return status;
    }

    public boolean editProfile(final String contact_no, final String email){
        class EditProfileAsync extends AsyncTask<Object, Object, Void> {
            String successString = null;

            @Override
            protected Void doInBackground(Object... params) {
                try {
                    RequestBody formBody = new FormBody.Builder()
                            .add("key",key)
                            .add("email_idd",email)
                            .add("contact_no",contact_no)
                            .add("owner_id",SPHelper.getSP(con,"owner_id"))
                            .build();

                    Log.d("Server Manager",email+" "+contact_no+" "+SPHelper.getSP(con,"owner_id"));
                    Request request = new Request.Builder()
                            .url("http://217.61.17.155/~appcrf/MedicineApp/pharma_app/edit_profile.php")
                            .method("POST",formBody)
                            .build();
                    Response response = httpClient.newCall(request).execute();
                    String sResponse = response.body().string();
                    Log.d("Server Manager",request.toString() + "\n"+ response + "\n" +sResponse);
                    sResponse = getProperObject(sResponse);
                    JSONObject jsonObject = new JSONObject(sResponse);
                    successString = jsonObject.optString("message");
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                progressDialog.dismiss();
            }
        }
        final EditProfileAsync editProfileAsync = new EditProfileAsync();
        editProfileAsync.execute();
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                while (editProfileAsync.successString == null){}
            }
        });
        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if(editProfileAsync.successString.compareTo("success") == 0){
            return true;
        }
        return false;
    }
}
