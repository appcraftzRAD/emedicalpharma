package com.appcraftz.medicallpharma;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.appcraftz.medicallpharma.Helpers.SPHelper;
import com.appcraftz.medicallpharma.ServerManager.ServerManager;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

    private static final String LOG_TAG = "LOGIN ACTIVITY";
    TextView mSignUp, mMobileNumber, mPassword;
    String sMobileNumber, sPassword;
    Button mLoginButton;
    private String TAG = "Login";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //Setting the title
        //getSupportActionBar().setTitle("Medicall");
        //getSupportActionBar().setSubtitle("Sign in to your account");
        Log.d(TAG, "Called " + getIntent().getExtras());
        Bundle notification_extras = null;
        if (getIntent().getExtras() != null) {
            notification_extras =getIntent().getExtras();
        }
        Log.d(LOG_TAG,SPHelper.getSP(this,"email_idd").toString());
        if((SPHelper.getSP(this,"email_idd").toString()).compareTo("none") !=0 && SPHelper.getSP(this,"email_idd").compareTo("") !=0){
            Intent toHome = new Intent(LoginActivity.this, HomeActivity.class);
            if(notification_extras != null)
                toHome.putExtras(notification_extras);
            startActivity(toHome);
            finish();
        }

        mMobileNumber = (TextView)findViewById(R.id.login_email);

        mPassword = (TextView)findViewById(R.id.password);

        mLoginButton = (Button)findViewById(R.id.loginbutton);
        mLoginButton.setOnClickListener(this);

        mSignUp = (TextView) findViewById(R.id.signuptext);
        mSignUp.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.loginbutton:
                ServerManager serverManager = new ServerManager(this);

                sMobileNumber=mMobileNumber.getText().toString().trim();
                sPassword = mPassword.getText().toString().trim();

                if(sMobileNumber.isEmpty() || sPassword.isEmpty()){
                    Toast.makeText(this, "Please fill the required fields!!", Toast.LENGTH_SHORT).show();
                    break;
                }

                boolean status = serverManager.login(sMobileNumber,sPassword);

                if(status){
                    Intent toHome = new Intent(LoginActivity.this, HomeActivity.class);
                    startActivity(toHome);
                    finish();
                }else{
                    Toast.makeText(getApplicationContext(),"Login Failed!!",Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.signuptext:
                Intent toSignUp = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(toSignUp);
                finish();
                break;
        }
    }


}
