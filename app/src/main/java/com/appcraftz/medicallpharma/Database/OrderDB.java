package com.appcraftz.medicallpharma.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.appcraftz.medicallpharma.Beans.Order;

import java.util.LinkedList;

/**
 * Created by silentcoder on 26/5/17.
 * Author: Sushil Maurya
 * Purpose: A Database for the orders
 */

public class OrderDB {
    final String TAG = "ORDER DATABASE";
    OrderDBHandler orderDBHandler;

    public OrderDB(Context context){
        orderDBHandler = new OrderDBHandler(context);
    }

    public long insert_order(String order_id,String user_id,String user_name,String datetime,String medicine_name,String quantity,String contact_number,String prescription,String station_name,
                      String train_number,String coach_number,long arrival_time,String status,String checked){
        SQLiteDatabase sqLiteDatabase = orderDBHandler.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(OrderDBHandler.ORDER_ID,order_id);
        contentValues.put(OrderDBHandler.USER_ID,user_id);
        contentValues.put(OrderDBHandler.USER_NAME,user_name);
        contentValues.put(OrderDBHandler.DATETIME,datetime);
        contentValues.put(OrderDBHandler.MEDICINE_NAME,medicine_name);
        contentValues.put(OrderDBHandler.QUANTITY,quantity);
        contentValues.put(OrderDBHandler.CONTACT_NUMBER,contact_number);
        contentValues.put(OrderDBHandler.PRESCRIPTION,prescription);
        contentValues.put(OrderDBHandler.STATION_NAME,station_name);
        contentValues.put(OrderDBHandler.TRAIN_NUMBER,train_number);
        contentValues.put(OrderDBHandler.COACH_NUMBER,coach_number);
        contentValues.put(OrderDBHandler.ARRIVAL_TIME,arrival_time);
        contentValues.put(OrderDBHandler.STATUS,status);
        contentValues.put(OrderDBHandler.CHECKED,checked);
        return sqLiteDatabase.insert(OrderDBHandler.TABLE_NAME,null,contentValues);
    }

    public long insert(Order order){
        SQLiteDatabase sqLiteDatabase = orderDBHandler.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(OrderDBHandler.ORDER_ID,order.getOrder_id());
        contentValues.put(OrderDBHandler.USER_ID,order.getUser_id());
        contentValues.put(OrderDBHandler.USER_NAME,order.getUser_name());
        contentValues.put(OrderDBHandler.DATETIME,order.getDatetime());
        contentValues.put(OrderDBHandler.MEDICINE_NAME,order.getMedicines());
        contentValues.put(OrderDBHandler.QUANTITY,order.getQuantity());
        contentValues.put(OrderDBHandler.CONTACT_NUMBER,order.getContact_no());
        contentValues.put(OrderDBHandler.PRESCRIPTION,order.getPrescription());
        contentValues.put(OrderDBHandler.STATION_NAME,order.getStation());
        contentValues.put(OrderDBHandler.TRAIN_NUMBER,order.getTrain_no());
        contentValues.put(OrderDBHandler.COACH_NUMBER,order.getCoach_no());
        contentValues.put(OrderDBHandler.ARRIVAL_TIME,order.getArrival_datetime());
        contentValues.put(OrderDBHandler.STATUS,order.getStatus());
        contentValues.put(OrderDBHandler.CHECKED,order.getChecked());
        return sqLiteDatabase.insert(OrderDBHandler.TABLE_NAME,null,contentValues);
    }

    public long update_order(String order_id,String  user_id,String user_name,String datetime,String medicine_name,String quantity,
                      String contact_number,String prescription,String station_name,
                      String train_number,String coach_number,String arrival_time,String status,String checked){
        SQLiteDatabase sqLiteDatabase = orderDBHandler.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(OrderDBHandler.ORDER_ID,order_id);
        contentValues.put(OrderDBHandler.USER_ID,user_id);
        contentValues.put(OrderDBHandler.USER_NAME,user_name);
        contentValues.put(OrderDBHandler.DATETIME,datetime);
        contentValues.put(OrderDBHandler.MEDICINE_NAME,medicine_name);
        contentValues.put(OrderDBHandler.QUANTITY,quantity);
        contentValues.put(OrderDBHandler.CONTACT_NUMBER,contact_number);
        contentValues.put(OrderDBHandler.PRESCRIPTION,prescription);
        contentValues.put(OrderDBHandler.STATION_NAME,station_name);
        contentValues.put(OrderDBHandler.TRAIN_NUMBER,train_number);
        contentValues.put(OrderDBHandler.COACH_NUMBER,coach_number);
        contentValues.put(OrderDBHandler.ARRIVAL_TIME,arrival_time);
        contentValues.put(OrderDBHandler.STATUS,status);
        contentValues.put(OrderDBHandler.CHECKED,checked);
        String where = OrderDBHandler.ORDER_ID+"=?";
        String[] arg = {String.valueOf(order_id)};
        return sqLiteDatabase.update(OrderDBHandler.TABLE_NAME,contentValues,where,arg);
    }

    public long update_order(Order order){
        return update_order(order.getOrder_id(),order.getUser_id(),order.getUser_name(),order.getDatetime(),order.getMedicines()
        ,order.getQuantity(),order.getContact_no(),order.getPrescription(),order.getStation()
        ,order.getTrain_no(),order.getCoach_no(),order.getArrival_datetime(),order.getStatus(),order.getChecked());
    }
    /**
     * @param status
     * for received orders status = null
     * for accepted orders status = accepted
     * for rejected orders status = rejected
     * @return
     */
    public LinkedList<Order> filterByStatus(String status){
        LinkedList<Order> filteredOrders = new LinkedList<>();
        SQLiteDatabase sqLiteDatabase = orderDBHandler.getReadableDatabase();
        String[] columns = {
                OrderDBHandler.ORDER_ID,
                OrderDBHandler.USER_ID,
                OrderDBHandler.USER_NAME,
                OrderDBHandler.DATETIME,
                OrderDBHandler.MEDICINE_NAME,
                OrderDBHandler.QUANTITY,
                OrderDBHandler.CONTACT_NUMBER,
                OrderDBHandler.PRESCRIPTION,
                OrderDBHandler.STATION_NAME,
                OrderDBHandler.TRAIN_NUMBER,
                OrderDBHandler.COACH_NUMBER,
                OrderDBHandler.ARRIVAL_TIME,
                OrderDBHandler.STATUS,
                OrderDBHandler.CHECKED,};
        String selection = OrderDBHandler.STATUS + " = ?";
        String[] selectionArgs = {status};
        String sortOrder =OrderDBHandler.DATETIME + " DESC";
        Cursor cursor = sqLiteDatabase.query(OrderDBHandler.TABLE_NAME,columns,selection,selectionArgs,null,null,sortOrder);
        addToContainer(cursor,filteredOrders);
        return filteredOrders;
    }

    private void addToContainer(Cursor cursor, LinkedList<Order> filteredOrders) {
        if(cursor.moveToFirst()){
            do {
                Order order = new Order();
                order.setOrder_id(cursor.getString(0));
                order.setUser_id(cursor.getString(1));
                order.setUser_name(cursor.getString(2));
                order.setDatetime(cursor.getString(3));
                order.setMedicines(cursor.getString(4));
                order.setQuantity(cursor.getString(5));
                order.setContact_no(cursor.getString(6));
                order.setPrescription(cursor.getString(7));
                order.setStation(cursor.getString(8));
                order.setTrain_no(cursor.getString(9));
                order.setCoach_no(cursor.getString(10));
                order.setArrival_time(cursor.getString(11));
                order.setStatus(cursor.getString(12));
                order.setChecked(cursor.getString(13));
                filteredOrders.add(order);
            } while (cursor.moveToNext());
        }
    }

    //Inner Handler Class
    class OrderDBHandler extends SQLiteOpenHelper{
        private static final int DATABASE_VERSION = 1;
        private static final String DATABASE_NAME = "Medicall";
        //Table Name
        private static final String TABLE_NAME = "OrderDetails";
        //Columns
        private static final String ORDER_ID = "order_id";
        private static final String DATETIME = "date_time";
        private static final String MEDICINE_NAME = "medicine_name";
        private static final String USER_ID = "user_id";
        private static final String USER_NAME = "user_name";
        private static final String QUANTITY = "quantity";
        private static final String CONTACT_NUMBER = "contact_number";
        private static final String PRESCRIPTION = "prescription";
        private static final String STATION_NAME = "station_name";
        private static final String TRAIN_NUMBER = "train_number";
        private static final String COACH_NUMBER = "coach_number";
        private static final String ARRIVAL_TIME = "arrival_time";
        private static final String STATUS = "status";
        private static final String RECEIVED ="received";
        private static final String CHECKED = "checked";
        private static final String CREATE_QUERY = "CREATE TABLE "+TABLE_NAME+" ( "+
                ORDER_ID+ " INTEGER PRIMARY KEY, "+
                USER_ID+" VARCHAR(10), " +
                USER_NAME+" VARCHAR(50), "+
                DATETIME+" VARCHAR(20), "+
                MEDICINE_NAME+" TEXT, "+
                QUANTITY+" TEXT, "+
                CONTACT_NUMBER+ " VARCHAR(20), "+
                PRESCRIPTION+" VARCHAR(255), "+
                STATION_NAME+" VARCHAR(50), " +
                TRAIN_NUMBER+" VARCHAR(10), "+
                COACH_NUMBER+" VARCHAR(10), "+
                ARRIVAL_TIME +" VARCHAR(20), "+
                STATUS+" VARCHAR(15), "+
                RECEIVED+ " VARCHAR(20), "+
                CHECKED+" TEXT);";
        //Context context;

        public OrderDBHandler(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
            //this.context = context;
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_QUERY);
            Log.d(TAG,"TABLE CREATED");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }
}
