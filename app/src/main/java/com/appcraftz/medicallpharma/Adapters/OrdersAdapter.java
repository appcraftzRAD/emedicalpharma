/**
 * Created by silentcoder on 16/5/17.
 * Author: Sushil Maurya
 * Purpose: Adapter for the RequestOrders
 */

package com.appcraftz.medicallpharma.Adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appcraftz.medicallpharma.Beans.Order;
import com.appcraftz.medicallpharma.HomeActivity;
import com.appcraftz.medicallpharma.R;

import java.util.List;

public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.OrdersViewHolder>{

    private List<Order> orderArrayList;

    public OrdersAdapter(List<Order> orderArrayList) {
        this.orderArrayList = orderArrayList;
    }

    /**
     * Inner class for ViewHolder Items
      */
    public static class OrdersViewHolder extends RecyclerView.ViewHolder{


        //Remember if any field is added update in the Order Beans Class as well as layout/single_order_item
        TextView tv_order_id;
        TextView tv_order_userid;
        TextView tv_order_quantity;
        TextView tv_order_contact_no;
        TextView tv_order_station;
        TextView tv_order_prescription;
        TextView tv_order_medicine_name;
        TextView tv_order_date;
        TextView tv_order_time;
        TextView tv_order_train_no;
        TextView tv_order_coach_no;
        TextView tv_order_arrival;
        TextView tv_order_status;
        Button btn_order_accept;
        Button btn_order_reject;
        //Button btn_order_details;
        TextView tv_order_details;
        LinearLayout order_ar;
        LinearLayout order_status;

        public OrdersViewHolder(View itemView) {
            super(itemView);
            //this.tv_order_medicine_name = (TextView) itemView.findViewById(R.id.tv_order_medicine_name);
            this.tv_order_userid = (TextView) itemView.findViewById(R.id.tv_order_userid);//for user_name
            //this.tv_order_quantity = (TextView) itemView.findViewById(R.id.tv_order_quantity);
            this.tv_order_contact_no = (TextView) itemView.findViewById(R.id.tv_order_contact_no);
            this.tv_order_station = (TextView) itemView.findViewById(R.id.tv_order_station);
            this.tv_order_id = (TextView) itemView.findViewById(R.id.tv_order_id);
            //this.tv_order_prescription = (TextView) itemView.findViewById(R.id.tv_order_prescription);
            this.tv_order_date = (TextView) itemView.findViewById(R.id.tv_order_date);
            this.tv_order_time = (TextView) itemView.findViewById(R.id.tv_order_time);
            this.tv_order_train_no = (TextView) itemView.findViewById(R.id.tv_order_train_no);
            //this.tv_order_coach_no = (TextView) itemView.findViewById(R.id.tv_order_coach_no);
            this.tv_order_arrival = (TextView) itemView.findViewById(R.id.tv_order_arrival);
            this.tv_order_status = (TextView) itemView.findViewById(R.id.tv_order_status);
            //this.btn_order_accept = (Button) itemView.findViewById(R.id.btn_order_accept);
            //this.btn_order_reject = (Button) itemView.findViewById(R.id.btn_order_reject);
            //this.btn_order_details = (Button) itemView.findViewById(R.id.btn_order_details);
            this.tv_order_details = (TextView) itemView.findViewById(R.id.tv_order_details);
            //this.order_ar= (LinearLayout) itemView.findViewById(R.id.ll_order_ar);
            this.order_status= (LinearLayout) itemView.findViewById(R.id.ll_order_status);

        }
    }

    @Override
    public OrdersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.single_order_item,parent,false);

        view.setOnClickListener(HomeActivity.orderOnClickListener);

        OrdersViewHolder ordersViewHolder = new OrdersViewHolder(view);

        return ordersViewHolder;
    }

    @Override
    public void onBindViewHolder(OrdersViewHolder holder, int position) {

        TextView tv_order_id=holder.tv_order_id;
        TextView tv_order_username =holder.tv_order_userid;
        //TextView tv_order_quantity=holder.tv_order_quantity;
        TextView tv_order_contact_no=holder.tv_order_contact_no;
        TextView tv_order_station=holder.tv_order_station;
        //TextView tv_order_prescription=holder.tv_order_prescription;
        //TextView tv_order_medicine_name=holder.tv_order_medicine_name;
        TextView tv_order_date=holder.tv_order_date;
        TextView tv_order_time=holder.tv_order_time;//After DB updated change accordingly
        TextView tv_order_train_no=holder.tv_order_train_no;
        //TextView tv_order_coach_no=holder.tv_order_coach_no;
        TextView tv_order_arrival=holder.tv_order_arrival;
        TextView tv_order_status = holder.tv_order_status;
        //Button btn_order_accept=holder.btn_order_accept;
        //Button btn_order_reject=holder.btn_order_reject;
       // Button btn_order_details=holder.btn_order_details;
        TextView tv_order_details=holder.tv_order_details;
        //LinearLayout order_ar = holder.order_ar;
        LinearLayout order_status = holder.order_status;

        Order currentOrder=orderArrayList.get(position);

        Log.d("KEY",currentOrder.toString());
        tv_order_id.setText(String.valueOf(currentOrder.getOrder_id()));
        tv_order_username.setText(currentOrder.getUser_name());
        //tv_order_quantity.setText(String.valueOf(currentOrder.getQuantity()));
        tv_order_contact_no.setText(currentOrder.getContact_no());
        tv_order_station.setText(currentOrder.getStation());
        //tv_order_prescription.setText("Click to View");

        //tv_order_prescription.setTag(currentOrder.getPrescription());

        //tv_order_medicine_name.setText(currentOrder.getMedicines());
        tv_order_train_no.setText(String.valueOf(currentOrder.getTrain_no()));
        //tv_order_coach_no.setText(currentOrder.getCoach_no());
        tv_order_date.setText(currentOrder.getDate());
        tv_order_time.setText(currentOrder.getTime());
        tv_order_arrival.setText(String.valueOf(currentOrder.getArrival_time()));
        //btn_order_accept.setOnClickListener(HomeActivity.orderOnClickListener);
        //btn_order_reject.setOnClickListener(HomeActivity.orderOnClickListener);
        //btn_order_details.setOnClickListener(HomeActivity.orderOnClickListener);
        //btn_order_details.setTag(currentOrder);
        tv_order_details.setOnClickListener(HomeActivity.orderOnClickListener);
        tv_order_details.setTag(currentOrder);
        //btn_order_accept.setTag(currentOrder.getUser_name());
        //btn_order_reject.setTag(currentOrder.getUser_name());
        //btn_order_accept.setTag(currentOrder);
        //btn_order_reject.setTag(currentOrder);
        //tv_order_prescription.setOnClickListener(HomeActivity.orderOnClickListener);
        String status = currentOrder.getStatus();


        Log.d("Order Adapter", status + currentOrder.getOrder_id());
        if( status.compareTo("null") != 0){
            //ViewGroup.LayoutParams layoutParams = order_ar.getLayoutParams();
            //layoutParams.height = 0;
            //order_ar.setLayoutParams(layoutParams);
            //order_ar.setVisibility(View.INVISIBLE);

            ViewGroup.LayoutParams layoutParams;
            layoutParams = order_status.getLayoutParams();
            tv_order_status.setText(status);
            layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
            order_status.setLayoutParams(layoutParams);
            order_status.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return orderArrayList.size();
    }
}
