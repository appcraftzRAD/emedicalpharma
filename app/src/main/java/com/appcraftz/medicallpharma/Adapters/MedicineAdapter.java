package com.appcraftz.medicallpharma.Adapters;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.appcraftz.medicallpharma.OrdersActivity;
import com.appcraftz.medicallpharma.R;

import java.nio.charset.CharacterCodingException;
import java.util.ArrayList;

/**
 * Created by silentcoder on 2/6/17.
 */

public class MedicineAdapter extends ArrayAdapter<String> {
    Context context;
    ArrayList<String> medicine,quantity;
    ArrayList<Boolean> checked;
    public MedicineAdapter(Context context, ArrayList<String> medicine, ArrayList<String> quantity) {
        super(context, R.layout.single_medicine_item, medicine);
        this.context = context;
        this.medicine = medicine;
        this.quantity = quantity;
        this.checked = new ArrayList<>();
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater= (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view=layoutInflater.inflate(R.layout.single_medicine_item,parent,false);
        TextView tv_order_medicine= (TextView) view.findViewById(R.id.tv_order_medicine);
        tv_order_medicine.setText(medicine.get(position));
        TextView tv_order_qnt= (TextView) view.findViewById(R.id.tv_order_qnt);
        tv_order_qnt.setText(quantity.get(position));
        CheckBox cb_order_medicine = (CheckBox) view.findViewById(R.id.cb_order_medicine);
        /*cb_order_medicine.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                checked.set(position,isChecked);
            }
        });*/
        return view;
    }
}
