package com.appcraftz.medicallpharma;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.appcraftz.medicallpharma.ServerManager.ServerManager;
import com.google.firebase.iid.FirebaseInstanceId;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "SignUpAvtivity";
    Button mSignedUp;
    TextView mLoginAgain;
    EditText ownername,contactnum,licencenum,shopname,emailid,address,area,pincode,password;
    String sownername,scontactnum,slicencenum,sshopname,semailid,saddress,sarea,spincode,spassword,stoken;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        initialize();

        mSignedUp = (Button)findViewById(R.id.signupbutton);
        mSignedUp.setOnClickListener(this);

        mLoginAgain = (TextView) findViewById(R.id.backtologin);
        mLoginAgain.setOnClickListener(this);
    }

    private void initialize() {

        //Setting the title
        //getSupportActionBar().setTitle("Pharmacy Registration");
        //getSupportActionBar().setSubtitle("Sign up");

        ownername = (EditText) findViewById(R.id.ownername);
        contactnum = (EditText) findViewById(R.id.contactnum);
        licencenum = (EditText) findViewById(R.id.licencenum);
        shopname = (EditText) findViewById(R.id.shopname);
        emailid = (EditText) findViewById(R.id.emailid);
        address = (EditText) findViewById(R.id.address);
        area = (EditText) findViewById(R.id.area); // Area is nearest_station
        pincode = (EditText) findViewById(R.id.pincode);
        password = (EditText) findViewById(R.id.password);

        Log.d(TAG,FirebaseInstanceId.getInstance().getId()+ " " + FirebaseInstanceId.getInstance().getToken());

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.signupbutton:
                sownername = ownername.getText().toString();
                scontactnum = contactnum.getText().toString();
                slicencenum = licencenum.getText().toString();
                sshopname = shopname.getText().toString();
                semailid = emailid.getText().toString();
                saddress = address.getText().toString();
                sarea = area.getText().toString();
                spincode = pincode.getText().toString();
                spassword = password.getText().toString();
                stoken = FirebaseInstanceId.getInstance().getToken();

                if(sownername.isEmpty() || scontactnum.isEmpty() || slicencenum.isEmpty() || sshopname.isEmpty()
                    || semailid.isEmpty() || saddress.isEmpty() || sarea.isEmpty() || spincode.isEmpty()
                        ||spassword.isEmpty()){
                    Toast.makeText(this, "Please fill the required fields!!", Toast.LENGTH_SHORT).show();
                    break;
                }
                ServerManager serverManager = new ServerManager(this);
                boolean status = serverManager.register(sownername,scontactnum,slicencenum,sshopname,
                        semailid,saddress,sarea,spincode,spassword,stoken);

                if(status == true){
                    Toast.makeText(getApplicationContext(),"Successfully Registered!!",Toast.LENGTH_LONG)
                        .show();
                    Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
                    startActivity(intent);
                    finish();
                }else{
                    Toast.makeText(getApplicationContext(),"Sorry! There was some error try again!!",Toast.LENGTH_LONG)
                            .show();
                }

                break;
            case R.id.backtologin:
                Intent toLoginActivity = new Intent(SignUpActivity.this, LoginActivity.class);
                startActivity(toLoginActivity);
                finish();
                break;
            default:

        }
    }
}
