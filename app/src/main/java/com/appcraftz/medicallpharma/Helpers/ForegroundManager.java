/**
 * Created by silentcoder on 18/5/17.
 * Author: Sushil Maurya
 * Purpose: Class for checking whether the app is in foreground or background
 */

package com.appcraftz.medicallpharma.Helpers;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class ForegroundManager implements Application.ActivityLifecycleCallbacks {


    public static final String TAG = "ForegroundManager";
    //singleton instance
    private static ForegroundManager instance;
    private boolean foreground, paused;
    private Handler handler;
    private List<Listener> listeners;
    private Runnable check;

    public interface Listener{
        public void onBecameForeground();
        public void onBecameBackground();
    }

    private ForegroundManager() {
        foreground =false;
        paused =true;
        handler = new Handler(Looper.getMainLooper());
        listeners =new CopyOnWriteArrayList<Listener>();
    }

    public static ForegroundManager init(Application application){
        if(instance == null){
            instance = new ForegroundManager();
            application.registerActivityLifecycleCallbacks(instance);
        }
        return instance;
    }

    public static ForegroundManager get(){
        if (instance == null) {
            throw new IllegalStateException(
                    "Foreground is not initialised - invoke " +
                            "at least once with parameterised init/get");
        }
        return instance;
    }

    public boolean isForeground(){
        return foreground;
    }

    public boolean isBackground(){
        return !foreground;
    }

    public void addListener(ForegroundManager.Listener listener){
        listeners.add(listener);
    }

    public void removeListener(ForegroundManager.Listener listener){
        listeners.remove(listener);
    }
    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {
        paused = false;
        boolean wasBackground = !foreground;
        foreground = true;

        if (check != null)
            handler.removeCallbacks(check);

        if (wasBackground){
            Log.i(TAG, "went foreground");
            for (Listener l : listeners) {
                try {
                    l.onBecameForeground();
                } catch (Exception exc) {
                    Log.e(TAG, "Listener threw exception!", exc);
                }
            }
        } else {
            Log.i(TAG, "still foreground");
        }
    }

    @Override
    public void onActivityPaused(Activity activity) {
        paused = true;
        if (check != null)
            handler.removeCallbacks(check);

        handler.postDelayed(check = new Runnable(){
            @Override
            public void run() {
                if (foreground && paused) {
                    foreground = false;
                    Log.i(TAG, "went background");
                    for (Listener l : listeners) {
                        try {
                            l.onBecameBackground();
                        } catch (Exception exc) {
                            Log.e(TAG, "Listener threw exception!", exc);
                        }
                    }
                } else {
                    Log.i(TAG, "still foreground");
                }
            }
        }, 500);
    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }
}

