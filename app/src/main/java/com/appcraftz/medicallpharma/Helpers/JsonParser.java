package com.appcraftz.medicallpharma.Helpers;


import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by appcraftz on 08/03/17.
 */

public class JsonParser {

    public ArrayList<String> parseCity(String JSON)
    {
        ArrayList<String> notices = new ArrayList<>();


        try {
            JSONObject rootObject = new JSONObject(JSON);

            JSONArray jsonArray = rootObject.getJSONArray("data");

            for(int i=0;i<jsonArray.length();i++)
            {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                notices.add(jsonObject.optString("city_name"));
            }
            return notices;

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void parseMedicine(String JSON,ArrayList<String> medicines,ArrayList<String> quantity) {
        try {
            JSONObject jsonObject = new JSONObject(JSON);
            JSONArray jsonArray = jsonObject.getJSONArray("medicines");
            for (int i=0;i<jsonArray.length();i++){
                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                medicines.add(jsonObject1.optString("name"));
                quantity.add(jsonObject1.optString("quantity"));
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public JSONArray formJson(ArrayList<String> medicines,ArrayList<String> checked){

        JSONArray jsonArray = new JSONArray();
        try {
            for (int i=0;i<medicines.size();i++){
                JSONObject jsonObject1 = new JSONObject();
                if(Boolean.parseBoolean(checked.get(i))){
                    jsonObject1.put("name",medicines.get(i));
                    jsonArray.put(jsonObject1);
                }
            }
            Log.d("JSONPARSER",jsonArray.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonArray;
    }
}
