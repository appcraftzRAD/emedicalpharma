/**
 * Created by silentcoder on 19/5/17.
 * Author: Sushil Maurya
 * Purpose: A Service which implements the on MessageReceived method to
 *          show notification if order arrives.
 */

package com.appcraftz.medicallpharma.Services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.appcraftz.medicallpharma.Beans.Order;
import com.appcraftz.medicallpharma.Database.OrderDB;
import com.appcraftz.medicallpharma.Helpers.ForegroundManager;
import com.appcraftz.medicallpharma.Helpers.JsonParser;
import com.appcraftz.medicallpharma.HomeActivity;
import com.appcraftz.medicallpharma.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Map;

public class OrderReceivingService extends FirebaseMessagingService {
    PendingIntent pendingIntent;
    String TAG = "Service";
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        ForegroundManager foregroundManager = ForegroundManager.init(getApplication());
        Intent intent = new Intent(this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        Log.i(TAG,"Inside onMessageReceived");
        if (foregroundManager.isForeground()) {
            /**
             * If in foreground and has data update the list ,
             * if data is not there just show the notification
             */
            pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                    PendingIntent.FLAG_ONE_SHOT);

            if (remoteMessage.getData().size() > 0) {
                Log.d(TAG,"RemoteMessage.getData()");
                Log.d(TAG,"else RemoteMessage.getData() " + remoteMessage.getData().get("name"));
                Map<String, String> remoteMessageData = remoteMessage.getData();
                //remoteMessageData.get("Key") according to the database
                String med = remoteMessageData.get("medicine_name");
                ArrayList<String> quantity = new ArrayList<String>();
                ArrayList<String> medicines = new ArrayList<String>();
                JsonParser jsonParser = new JsonParser();
                jsonParser.parseMedicine(med,medicines,quantity);
                Order currentOrder = new Order(remoteMessageData.get("order_id"),
                        remoteMessageData.get("user_id"),
                        remoteMessageData.get("name"),//user_name
                        medicines.toString(),
                        quantity.toString(),
                        remoteMessageData.get("prescription"),
                        remoteMessageData.get("station"),
                        remoteMessageData.get("contact_no"),
                        remoteMessage.getData().get("train_no"),
                        remoteMessage.getData().get("arrival_time"),
                        remoteMessage.getData().get("coach_no"),
                        remoteMessage.getData().get("order_date/time"));
                currentOrder.setStatus("null");
                currentOrder.setChecked(false,medicines.size());
                OrderDB orderDB = new OrderDB(getApplicationContext());
                orderDB.insert(currentOrder);
                ((LinkedList<Order>)HomeActivity.receivedOrders).addFirst(currentOrder);

                HomeActivity.notifyData = true;
                //Update Local Database
            }
        } else {
            /**
             * If in background show the notification and if data present add the intent with extras
             */


            //In case if activity is in background and data is present
            Log.i(TAG,"else RemoteMessage.getData() " + remoteMessage.getData().get("order_date/time"));
            if (remoteMessage.getData().size() > 0){
                intent.putExtra("order_id",remoteMessage.getData().get("order_id"));
                intent.putExtra("user_id", remoteMessage.getData().get("user_id"));
                intent.putExtra("name", remoteMessage.getData().get("name"));
                intent.putExtra("medicine_name", remoteMessage.getData().get("medicine_name"));
                intent.putExtra("prescription",remoteMessage.getData().get("prescription"));
                intent.putExtra("station",remoteMessage.getData().get("station"));
                intent.putExtra("contact_no", remoteMessage.getData().get("contact_no"));
                intent.putExtra("train_no",remoteMessage.getData().get("train_no"));
                intent.putExtra("arrival_time",remoteMessage.getData().get("arrival_time"));
                intent.putExtra("coach_no",remoteMessage.getData().get("coach_no"));
                intent.putExtra("received",remoteMessage.getData().get("received"));
                intent.putExtra("order_date/time",remoteMessage.getData().get("order_date/time"));//Added
            }
            pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                    PendingIntent.FLAG_ONE_SHOT);

        }
        showNotification(remoteMessage);
    }

    //Customize if required

    private void showNotification(RemoteMessage remoteMessage) {
        Log.d(TAG,"showing Notif");

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);
            if(remoteMessage.getNotification() != null) {
                notificationBuilder.setContentTitle(remoteMessage.getNotification().getTitle());
                notificationBuilder.setContentInfo(remoteMessage.getNotification().getBody());
            }else{
                notificationBuilder.setContentTitle("New Request");
                notificationBuilder.setContentInfo(remoteMessage.getData().get("order_id"));
            }
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());

    }
}
