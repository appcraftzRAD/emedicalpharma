package com.appcraftz.medicallpharma.Beans;

import java.io.Serializable;
import java.util.ArrayList;
import android.util.Log;

/**
 * Created by silentcoder on 16/5/17.
 * Author: Sushil Maurya
 * Purpose: Simple beans class for the orders
 */

public class Order implements Serializable{

    String user_name;
    /**
     * Remember to match with database
     * ,match with the res/layout/single_order_item.xml
     * and also with the Adapters/OrdersAdapter Class
     * Add date and time when db updated
     * */
    String order_id;
    String user_id;
    String medicines;
    String quantity;
    String prescription;
    String station;
    String contact_no;
    String shop_name;
    long received;
    String train_no;
    String datetime;
    String arrival_time;
    String coach_no;
    String checked;
    String status = null;

    public Order(){}

    public Order(String order_id, String user_id, String user_name, String medicines, String quantity,
                 String prescription, String station, String contact_no, String shop_name,
                 long received, String train_no, String arrival_time, String coach_no) {
        this.order_id = order_id;
        this.user_id = user_id;
        this.user_name = user_name;
        this.medicines = medicines;
        this.quantity = quantity;
        this.prescription = prescription;
        this.station = station;
        this.contact_no = contact_no;
        this.shop_name = shop_name;
        this.received = received;
        this.train_no = train_no;
        this.arrival_time = arrival_time;
        this.coach_no = coach_no;
    }


    public Order(String order_id, String user_id, String user_name, String medicines, String quantity,
                 String prescription, String station, String contact_no, String train_no,
                 String arrival_time, String coach_no, String datetime) {
        this.order_id = order_id;
        this.user_id = user_id;
        this.user_name = user_name;
        this.medicines = medicines;
        this.quantity = quantity;
        this.prescription = prescription;
        this.station = station;
        this.contact_no = contact_no;
        this.train_no = train_no;
        this.arrival_time = arrival_time;
        this.coach_no = coach_no;
        this.datetime = datetime;
    }



    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getMedicines() {
        return medicines;
    }

    public void setMedicines(String medicines) {
        this.medicines = medicines;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPrescription() {
        return prescription;
    }

    public void setPrescription(String prescription) {
        this.prescription = prescription;
    }

    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }

    public String getContact_no() {
        return contact_no;
    }

    public void setContact_no(String contact_no) {
        this.contact_no = contact_no;
    }
    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public long getReceived() {
        return received;
    }

    public void setReceived(long received) {
        this.received = received;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTrain_no() {
        return train_no;
    }

    public void setTrain_no(String train_no) {
        this.train_no = train_no;
    }

    public void setArrival_time(String arrival_time) {
        this.arrival_time = arrival_time;
    }

    public String getCoach_no() {
        return coach_no;
    }

    public void setCoach_no(String coach_no) {
        this.coach_no = coach_no;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getDate(){
        String[] dt = datetime.split(" ");
        Log.d("ORDER",datetime);
        return dt[0];
        //return datetime;
    }
    public String getTime(){
        String[] dt = datetime.split(" ");
        return dt[1].substring(0,5);
        //return "14:00";
        //return datetime;
    }
    public String getArrival_date(){
        String[] dt = arrival_time.split(" ");
        return dt[0];
        //return arrival_time;
    }
    public String getArrival_time() {
        String[] dt = arrival_time.split(" ");
        return dt[1].substring(0,5);
        //return  arrival_time;
    }
    public String getArrival_datetime() {
        //String[] dt = arrival_time.split(" ");
        //return dt[1].substring(0,5);
        return  arrival_time;
    }


    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String toString(){
        return  user_name+" "+
                order_id+" "+
                user_id+" "+
                medicines+" "+
                quantity+" "+
                prescription+" "+station+" "+contact_no+" "+received+" "+train_no+" "+datetime+" "+
                arrival_time+" "+coach_no+" "+status;
    }

    public void setChecked(boolean b, int size) {
        ArrayList<Boolean> checked = new ArrayList<Boolean>();
        for(int i=0;i<size;i++){
            checked.add(b);
        }
        this.checked = checked.toString();
    }
    public void setChecked(String checked){
        this.checked = checked;
    }
    public String getChecked(){
        return checked;
    }
}
