package com.appcraftz.medicallpharma;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.appcraftz.medicallpharma.Adapters.OrdersAdapter;
import com.appcraftz.medicallpharma.Beans.Order;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

import com.appcraftz.medicallpharma.Database.OrderDB;
import com.appcraftz.medicallpharma.Helpers.ForegroundManager;
import com.appcraftz.medicallpharma.Helpers.SPHelper;
import com.appcraftz.medicallpharma.Helpers.JsonParser;
import com.appcraftz.medicallpharma.ServerManager.ServerManager;
import com.google.firebase.iid.FirebaseInstanceId;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "HomeActivity";
    public static boolean notifyData = false;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private OrdersAdapter ordersAdapter;
    public static View.OnClickListener orderOnClickListener;
    public static List<Order> receivedOrders = new LinkedList<>();
    public static List<Order> acceptedOrders = new LinkedList<>();
    public static List<Order> rejectedOrders = new LinkedList<>();
    TextView nav_heading,nav_subheading;
    final int header_index = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        OrderDB orderDB = new OrderDB(getApplicationContext());

        //Start ForegroundManager
        ForegroundManager.init(getApplication());

        orderOnClickListener = new HomeActivity.OrderOnClickListener(this);

        //Initialize Recycler View and the layout Manager
        recyclerView = (RecyclerView) findViewById(R.id.orders_recycler_view);
        Log.d("Home Activity", FirebaseInstanceId.getInstance().getToken());

        layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);

        // Remember to change the animator if needed
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        /**
         * Delete this createDummyOrders created just for testing purpoe
         */
        //createDummyOrders();
        receivedOrders.clear();
        receivedOrders.addAll(orderDB.filterByStatus("null"));

        ordersAdapter = new OrdersAdapter(receivedOrders);

        recyclerView.setAdapter(ordersAdapter);

        notifyThread();
        //Update the view
        if (getIntent().getExtras() != null) {
            Log.d(TAG,"Intent Data " + (String) getIntent().getExtras().get("order_id"));

            String med = (String)getIntent().getExtras().get("medicine_name");
            ArrayList<String> quantity = new ArrayList<String>();
            ArrayList<String> medicines = new ArrayList<String>();
            ArrayList<Boolean> checked = new ArrayList<Boolean>();
            JsonParser jsonParser = new JsonParser();
            jsonParser.parseMedicine(med,medicines,quantity);
            Order currentOrder = new Order((String) getIntent().getExtras().get("order_id"),
                    (String)getIntent().getExtras().get("user_id"),
                    (String)getIntent().getExtras().get("name"),
                    (String)medicines.toString(),
                    (String)quantity.toString(),
                    (String)getIntent().getExtras().get("prescription"),
                    (String)getIntent().getExtras().get("station"),
                    (String)getIntent().getExtras().get("contact_no"),
                    (String)getIntent().getExtras().get("train_no"),
                    (String)getIntent().getExtras().get("arrival_time"),
                    (String)getIntent().getExtras().get("coach_no"),
                    (String)getIntent().getExtras().get("order_date/time"));
            currentOrder.setStatus("null");
            currentOrder.setChecked(false,medicines.size());
            ((LinkedList<Order>) receivedOrders).addFirst(currentOrder);
            orderDB.insert(currentOrder);
            //((LinkedList<Order>)receivedOrders).addFirst(o);
            //Log.d(TAG,receivedOrders.get(0).getMedicines());
            notifyData =true;
        }

        //Setting the title
        getSupportActionBar().setTitle("Medicall");
        getSupportActionBar().setSubtitle("Medicine Requests");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.getMenu().getItem(0).setChecked(true);
        View hview=navigationView.getHeaderView(header_index);
        nav_heading = (TextView) hview.findViewById(R.id.nav_heading);
        nav_subheading = (TextView) hview.findViewById(R.id.nav_subheading);
        nav_subheading.setText(SPHelper.getSP(this,"email_idd"));
        nav_heading.setText(SPHelper.getSP(this,"shop_name"));
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void notifyThread() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while(true){
                    if(notifyData == true){
                        ordersAdapter.notifyDataSetChanged();
                        notifyData = false;
                    }
                }
            }
        });
    }

    //To Delete
    private void createDummyOrders() {
        String[] medicineName = {"Acetaminophen",
                "Adderall",
                "Alprazolam",
                "Amitriptyline",
                "Amlodipine",
                "Amoxicillin",
                "Ativan",
                "Atorvastatin",
                "Azithromycin",
                "Ciprofloxacin"};
        String[] userName = {"sushilm2011",
                "sahirmakkar",
                "varsha67",
                "parthwaidya",
                "jyoti567",
                "vishalsingh2",
                "sushilm201",
                "sahilmak",
                "achintya23",
                "rajababu"};

        String[] contactNumber = {"8420906810",
                "7003215776",
                "9830449454",
                "8402563145",
                "7321546987",
                "8420579613",
                "9824563178",
                "9143278766",
                "8421569732",
                "8479651232"};
        int[] quantity = {5,4,8,9,10,1,2,4,5,6,7};
        long[] order_id = {1124536,1245344,1458769,1254698,1452365,1452361,1452644,1452365,1478951,1458796};
        String[] station = {"Agra City",
                "Howrah",
                "Delhi",
                "Allahabad City",
                "Laksar Junction",
                "Tadwal",
                "Tarana Road",
                "Kota",
                "Udaipur City",
                "Ramgarh"};
        long[] trainNumber = {3624530,7685767,1458769,1254698,1452365,1452361,1452644,1452365,1478951,1458796};
        long[] arrivalTime = {3624530,7685767,1458769,1254698,1452365,1452361,1452644,1452365,1478951,1458796};
        String[] coachNumber = {"S1",
                "A5",
                "S7",
                "A8",
                "D7",
                "D8",
                "S6",
                "S9",
                "S12",
                "B6"};
        for(int j=0;j<10;j++){
            receivedOrders.add(new Order(String.valueOf(order_id[j]),
                    userName[j],
                    "Sushil",
                    medicineName[j],
                    String.valueOf(quantity[j]),
                    "https://en.wikipedia.org/wiki/Portable_Network_Graphics#/media/File:PNG_transparency_demonstration_1.png",
                    station[j],
                    contactNumber[j],
                    String.valueOf(trainNumber[j]),
                    String.valueOf(arrivalTime[j]),
                    coachNumber[j],"12:59 17/05/17"));
        }
    }

    /**
     * Innerclass for handling on Order Click
     */
    private class OrderOnClickListener implements View.OnClickListener {

        private final Context context;

        private OrderOnClickListener(Context context){
            this.context=context;
        }

        @Override
        public void onClick(View v) {
            ServerManager serverManager = new ServerManager(getApplicationContext());
            String sOrder_id,sUser_name,status;
            sOrder_id = ((TextView) v.getRootView().findViewById(R.id.tv_order_id)).getText().toString();
            sUser_name = ((TextView) v.getRootView().findViewById(R.id.tv_order_userid)).getText().toString();
            Log.d(TAG,"Clicked " + v.getRootView());
            switch (v.getId()){
                case R.id.tv_order_details:
                    Pair<View,String>[] pair = new Pair[10];
                    pair[0] = new Pair<View,String>((TextView)v.getRootView().findViewById(R.id.tv_order_station),"station_transition");
                    pair[1] = new Pair<View, String>((TextView)v.getRootView().findViewById(R.id.tv_order_userid),"username_transition");
                    pair[2] = new Pair<View, String>((TextView)v.getRootView().findViewById(R.id.tv_order_arrival),"arrival_transition");
                    pair[3] = new Pair<View, String>((TextView)v.getRootView().findViewById(R.id.tv_order_contact_no),"contact_transition");
                    pair[4] = new Pair<View, String>((TextView)v.getRootView().findViewById(R.id.tv_order_train_no),"train_transition");
                    pair[5] = new Pair<View, String>((TextView)v.getRootView().findViewById(R.id.ctv_order_train_no),"tv_train_transition");
                    pair[6] = new Pair<View, String>((TextView)v.getRootView().findViewById(R.id.ctv_order_station),"tv_station_transition");
                    pair[7] = new Pair<View, String>((TextView)v.getRootView().findViewById(R.id.ctv_order_userid),"tv_username_transition");
                    pair[8] = new Pair<View, String>((TextView)v.getRootView().findViewById(R.id.ctv_order_arrival),"tv_arrival_transition");
                    pair[9] = new Pair<View, String>((TextView)v.getRootView().findViewById(R.id.ctv_order_contact_no),"tv_contact_transition");

                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                        ActivityOptions options;
                        options = ActivityOptions.makeSceneTransitionAnimation((Activity) v.getContext(),pair);
                        Intent i = new Intent(getApplication(), OrdersActivity.class);
                        i.putExtra("Order", (Order) v.getTag());
                        startActivity(i,options.toBundle());
                    }else {
                        Intent i = new Intent(getApplication(), OrdersActivity.class);
                        i.putExtra("Order", (Order) v.getTag());
                        startActivity(i);
                    }
                    break;
                /*case R.id.btn_order_accept:
                    status = "ACCEPT";
                    //status = serverManager.notifyUser(sOrder_id,((Order)v.getTag()).getUser_id(),sUser_name,status);
                    //Log.d(TAG,((Order)v.getTag()).getUser_id()+" " + ((Order)v.getTag()).getStatus());
                    status = "ACCEPTED";
                    if(status == null){

                    }else{
                        status = "ACCEPTED";//Update DB with this
                        ((Order)v.getTag()).setStatus("ACCEPTED");
                        OrderDB orderDB = new OrderDB(getApplicationContext());
                        orderDB.update_order(((Order)v.getTag()));
                        changeCard(v,status);
                    }

                    break;
                case R.id.btn_order_reject:
                    status = "REJECT";
                    //status = serverManager.notifyUser(sOrder_id,((Order)v.getTag()).getUser_id(),sUser_name,status);
                    status = "REJECTED";
                    if(status == null){

                    }else{
                        status = "REJECTED";//Update DB with this
                        ((Order)v.getTag()).setStatus("REJECTED");
                        OrderDB orderDB = new OrderDB(getApplicationContext());
                        orderDB.update_order(((Order)v.getTag()));
                        changeCard(v,status);
                    }
                    break;
                case R.id.tv_order_prescription:
                    String prescriptionUrl = (String) v.getTag();
                    Intent intent = new Intent(getApplicationContext(), PrescriptionDialog.class);
                    intent.putExtra("url",prescriptionUrl);
                    startActivity(intent);*/
            }
        }
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_received) {
            getSupportActionBar().setSubtitle("Medicine Requests");
            receivedOrders.clear();
            receivedOrders.addAll((new OrderDB(getApplicationContext())).filterByStatus("null"));
            ordersAdapter = new OrdersAdapter(receivedOrders);
            recyclerView.setAdapter(ordersAdapter);
            ordersAdapter.notifyDataSetChanged();

        } else if (id == R.id.nav_accepted) {
            getSupportActionBar().setSubtitle("Accepted Orders");
            acceptedOrders.clear();
            acceptedOrders.addAll((new OrderDB(getApplicationContext())).filterByStatus("ACCEPTED"));
            ordersAdapter = new OrdersAdapter(acceptedOrders);
            recyclerView.setAdapter(ordersAdapter);
            ordersAdapter.notifyDataSetChanged();

        } else if (id == R.id.nav_rejected) {
            getSupportActionBar().setSubtitle("Rejected Orders");
            rejectedOrders.clear();
            rejectedOrders.addAll((new OrderDB(getApplicationContext())).filterByStatus("REJECTED"));
            ordersAdapter = new OrdersAdapter(rejectedOrders);
            recyclerView.setAdapter(ordersAdapter);
            ordersAdapter.notifyDataSetChanged();

        } else if (id == R.id.nav_account) {
            Intent intent = new Intent(this,Profile.class);
            startActivity(intent);

        } else if (id == R.id.nav_logout) {
            SPHelper.setSP(this,"email_idd","");
            Intent intent = new Intent(this,LoginActivity.class);
            startActivity(intent);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
