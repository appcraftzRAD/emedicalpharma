package com.appcraftz.medicallpharma;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.appcraftz.medicallpharma.Helpers.SPHelper;
import com.appcraftz.medicallpharma.ServerManager.ServerManager;

public class EditProfile extends AppCompatActivity {

    EditText update_pro_email,update_pro_contact_no;
    Button Update;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        update_pro_contact_no = (EditText) findViewById(R.id.update_pro_contact_no);
        update_pro_contact_no.setText(SPHelper.getSP(this,"contact_no"));
        update_pro_email = (EditText) findViewById(R.id.update_pro_email_id);
        update_pro_email.setText(SPHelper.getSP(this,"email_idd"));
        Update = (Button) findViewById(R.id.update_pro);
        Update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String contact_no = update_pro_contact_no.getText().toString();
                String email_idd = update_pro_email.getText().toString();
                String textToShow = "Update Profile Failed!! Try Again after sometime..";
                ServerManager serverManager = new ServerManager(EditProfile.this);
                if(serverManager.editProfile(contact_no,email_idd)) {
                    SPHelper.setSP(getApplicationContext(), "email_idd", email_idd);
                    SPHelper.setSP(getApplicationContext(), "contact_no", contact_no);
                    textToShow = "Update successfull";
                }
                Toast.makeText(getApplicationContext(),textToShow,Toast.LENGTH_LONG).show();
                Intent intent = new Intent(EditProfile.this, Profile.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
