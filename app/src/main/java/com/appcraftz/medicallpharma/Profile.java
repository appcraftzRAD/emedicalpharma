package com.appcraftz.medicallpharma;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.appcraftz.medicallpharma.Helpers.SPHelper;
import com.google.firebase.iid.FirebaseInstanceId;

import org.w3c.dom.Text;

public class Profile extends AppCompatActivity {

    Button editProfile;
    TextView ownername,contactnum,licensenum,shopname,emailid,address,area,pincode;
    String sownername,scontactnum,slicencenum,sshopname,semailid,saddress,sarea,spincode,spassword,stoken;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        init();
        ownername.setText(SPHelper.getSP(this,"owner_name"));
        contactnum.setText(SPHelper.getSP(this,"contact_no"));
        licensenum.setText(SPHelper.getSP(this,"license_no"));
        shopname.setText(SPHelper.getSP(this,"shop_name"));
        emailid.setText(SPHelper.getSP(this,"email_idd"));
        address.setText(SPHelper.getSP(this,"address"));
        area.setText(SPHelper.getSP(this,"area"));
        pincode.setText(SPHelper.getSP(this,"pin_code"));

        editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Fire Dialog
                Intent intent = new Intent(getApplicationContext(),EditProfile.class);
                startActivity(intent);
                finish();
            }
        });

    }

    private void init(){

            //Setting the title
            getSupportActionBar().setTitle("Edit Profile");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

            ownername = (TextView) findViewById(R.id.pro_owner_name);
            contactnum = (TextView) findViewById(R.id.pro_contact_no);
            licensenum = (TextView) findViewById(R.id.pro_license_no);
            shopname = (TextView) findViewById(R.id.pro_shop_name);
            emailid = (TextView) findViewById(R.id.pro_email_id);
            address = (TextView) findViewById(R.id.pro_address);
            area = (TextView) findViewById(R.id.pro_nearest_station); // Area is nearest_station
            pincode = (TextView) findViewById(R.id.pro_pincode);
            editProfile = (Button) findViewById(R.id.pro_edit_profile);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
