/**
 * Author: Sushil Maurya
 * Purpose: Sets the Request Orders on the screen
 */

package com.appcraftz.medicallpharma;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appcraftz.medicallpharma.Adapters.MedicineAdapter;
import com.appcraftz.medicallpharma.Beans.Order;
import com.appcraftz.medicallpharma.Database.OrderDB;
import com.appcraftz.medicallpharma.Dialogs.PrescriptionDialog;
import com.appcraftz.medicallpharma.Helpers.JsonParser;
import com.appcraftz.medicallpharma.Helpers.SPHelper;
import com.appcraftz.medicallpharma.ServerManager.ServerManager;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class OrdersActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String LOG_TAG= "OrdersActivity";

    MedicineAdapter medicineAdapter;
    ArrayList<String> medicines,quantity;
    ArrayList<String> checked;
    LinearLayout medicineList;
    TextView tvdet_username,tvdet_date,tvdet_time,tvdet_contact,tvdet_train_no,tvdet_coach_no
            ,tvdet_station,tvdet_arrivaldate,tvdet_arrivaltime,tvdet_prescription;
    Button accept,reject;
    Order associated_order = null;
    String status;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);

        if(getIntent().getExtras()!=null){
            associated_order = (Order)getIntent().getSerializableExtra("Order");
        }else{
            Log.d(LOG_TAG,"No Intent");
        }

        status = associated_order.getStatus();
        init();

        String med = associated_order.getMedicines();
        String qty = associated_order.getQuantity();
        String chk = associated_order.getChecked();

        quantity = new ArrayList<String>(Arrays.asList(qty.substring(1,qty.length()-1).split(", ")));
        medicines = new ArrayList<String>(Arrays.asList(med.substring(1,med.length()-1).split(", ")));
        checked = new ArrayList<String>(Arrays.asList(chk.substring(1,chk.length()-1).split(", ")));
        Log.d(LOG_TAG,checked.toString());
        //createDummy();

        medicineAdapter = new MedicineAdapter(getApplicationContext(),medicines,quantity);
        int medicine_count = medicineAdapter.getCount();
        medicineList = (LinearLayout) findViewById(R.id.order_medicine_list);
        for(int i=0;i<medicine_count ;i++){
            View v = medicineAdapter.getView(i,null,medicineList);
            final int pos = i;
            CheckBox checkBox = (CheckBox)v.findViewById(R.id.cb_order_medicine);
            Log.d(LOG_TAG,status);
            if(status.compareTo("null")!=0){
                checkBox.setChecked(Boolean.parseBoolean(checked.get(i)));
                checkBox.setEnabled(false);
            }
            else {
                checkBox.setOnCheckedChangeListener(
                        new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                checked.set(pos,String.valueOf(isChecked));
                                Toast.makeText(getApplicationContext(), "Checked " + pos, Toast.LENGTH_SHORT).show();
                            }
                        });
            }
            checkBox.setVisibility(View.INVISIBLE);
            medicineList.addView(v);
        }
        Log.d(LOG_TAG,checked.toString());
        tvdet_prescription.setOnClickListener(this);
        accept.setOnClickListener(this);
        reject.setOnClickListener(this);
    }

    private void init() {
        getSupportActionBar().setTitle("Order");
        getSupportActionBar().setSubtitle(associated_order.getOrder_id());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        tvdet_date = (TextView) findViewById(R.id.tvdet_order_date);
        tvdet_time = (TextView) findViewById(R.id.tvdet_order_time);
        tvdet_username = (TextView) findViewById(R.id.tvdet_order_username);
        tvdet_contact = (TextView) findViewById(R.id.tvdet_order_contact_no);
        tvdet_train_no = (TextView) findViewById(R.id.tvdet_order_train_no);
        tvdet_coach_no = (TextView) findViewById(R.id.tvdet_order_coach_no);
        tvdet_station = (TextView) findViewById(R.id.tvdet_order_station);
        tvdet_arrivaldate = (TextView) findViewById(R.id.tvdet_order_arrival_date);
        tvdet_arrivaltime = (TextView) findViewById(R.id.tvdet_order_arrival_time);
        tvdet_prescription = (TextView) findViewById(R.id.tvdet_order_prescription);
        accept = (Button) findViewById(R.id.btn_order_accept);
        reject = (Button) findViewById(R.id.btn_order_reject);
        //set values

        tvdet_username.setText(associated_order.getUser_name());
        tvdet_contact.setText(associated_order.getContact_no());
        tvdet_date.setText(associated_order.getDate());
        tvdet_time.setText(associated_order.getTime());
        tvdet_train_no.setText(associated_order.getTrain_no());
        tvdet_coach_no.setText(associated_order.getCoach_no());
        tvdet_station.setText(associated_order.getStation());
        tvdet_arrivaldate.setText(associated_order.getArrival_date());
        tvdet_arrivaltime.setText(associated_order.getArrival_time());

        if(status.compareTo("null")!=0){
            setStatusInfo();
        }

    }

    private void setStatusInfo() {
        LinearLayout order_ar= (LinearLayout) findViewById(R.id.ll_order_ar);
        ViewGroup.LayoutParams layoutParams = order_ar.getLayoutParams();
        layoutParams.height = 0;
        order_ar.setLayoutParams(layoutParams);
        order_ar.setVisibility(View.INVISIBLE);
        LinearLayout order_status= (LinearLayout) findViewById(R.id.ll_order_status);
        layoutParams = order_status.getLayoutParams();
        ((TextView)findViewById(R.id.tv_order_status)).setText(status);
        layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        order_status.setLayoutParams(layoutParams);
        order_status.setVisibility(View.VISIBLE);
    }

    private void createDummy() {
        medicines.add("Acetaminophen");
        medicines.add("Adderall");
        medicines.add("Alprazolam");
        medicines.add("Amitriptyline");
        medicines.add("Amlodipine");
        medicines.add("Amoxicillin");
        medicines.add("Ativan");
        medicines.add("Atorvastatin");
        medicines.add("Azithromycin");
        medicines.add("Ciprofloxacin");

        quantity.add("1");
        quantity.add("3");
        quantity.add("5");
        quantity.add("7");
        quantity.add("7");
        quantity.add("2");
        quantity.add("3");
        quantity.add("5");
        quantity.add("4");
        quantity.add("10");

        for(int i=0;i<10;i++);
            //checked.add(false);
        //checked.set(6,true);
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tvdet_order_prescription:
                Intent intent = new Intent(getApplicationContext(), PrescriptionDialog.class);
                intent.putExtra("url",associated_order.getPrescription());
                startActivity(intent);
                break;
            case R.id.btn_order_accept:
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Are you sure?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        JSONArray jsonArray = new JSONArray();
                        checked.clear();
                        checked.addAll(Collections.nCopies(medicines.size(), String.valueOf(true)));
                        jsonArray = new JsonParser().formJson(medicines,checked);
                        ServerManager serverManager = new ServerManager(getApplicationContext());
                        //TODO: provide an accept all button
                        //TODO: check if SPHelper class has the required value
                        String result = serverManager.notifyUser(associated_order.getOrder_id(),
                                SPHelper.getSP(getApplicationContext(),"shop_name"),
                                jsonArray);
                        showResultandUpdateView(result);
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
                break;
            case R.id.btn_order_reject:
                final AlertDialog.Builder rbuilder = new AlertDialog.Builder(this);
                rbuilder.setMessage("Are you sure?");
                rbuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        checked.clear();
                        checked.addAll(Collections.nCopies(medicines.size(), String.valueOf(false)));
                        status="REJECTED";
                        associated_order.setChecked(checked.toString());
                        associated_order.setStatus(status);
                        OrderDB orderDB = new OrderDB(getApplicationContext());
                        orderDB.update_order(associated_order);
                        setStatusInfo();
                        overridePendingTransition( 0, 0);
                        startActivity(getIntent());
                        overridePendingTransition( 0, 0);
                        finish();
                    }
                });
                rbuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog rdialog = rbuilder.create();
                rdialog.show();
                break;
        }
    }

    private void showResultandUpdateView(String result) {
        if(result.compareTo("failed") == 0){
            //TODO: Notify SHOP TO REDO
            Toast.makeText(getApplicationContext(),"Try Again!! Error Occured!",Toast.LENGTH_LONG).show();
            return;
        }
        result.trim();
        result = result.substring(1,result.length()-1);
        if(result.isEmpty()){
            Toast.makeText(getApplicationContext(),"Already accepted by Other Pharmacy!",Toast.LENGTH_LONG).show();
            checked.clear();
            checked.addAll(Collections.nCopies(medicines.size(), String.valueOf(false)));
            status="REJECTED";
        }else {
            ArrayList<String> accepted_medicines = new ArrayList<>(Arrays.asList(
                    result.split(",")));
            Toast.makeText(getApplicationContext(),"Successfully Accepted Order",Toast.LENGTH_LONG).show();
            checked.clear();
            checked.addAll(Collections.nCopies(medicines.size(), String.valueOf(false)));
            //TODO: MODIFY THIS
            for (int i=0;i<accepted_medicines.size();i++){
                String am = accepted_medicines.get(i).substring(1,accepted_medicines.get(i).length()-1);
                for (int i1=0;i1<medicines.size();i1++){
                    Log.d(LOG_TAG,medicines.get(i1));
                    if(medicines.get(i1).trim().compareTo(am) == 0){
                        checked.set(i1,String.valueOf(true));
                        Log.d(LOG_TAG,"Executed");
                        break;
                    }
                }
            }
            status="ACCEPTED";
        }
        Log.d(LOG_TAG,checked.toString());
        associated_order.setChecked(checked.toString());
        associated_order.setStatus(status);
        OrderDB orderDB = new OrderDB(getApplicationContext());
        orderDB.update_order(associated_order);
        setStatusInfo();
        finish();
        overridePendingTransition( 0, 0);
        startActivity(getIntent());
        overridePendingTransition( 0, 0);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            finishAfterTransition();
            startActivity(new Intent(getApplicationContext(),HomeActivity.class));
        }
        return true;
    }

    @Override
    public void recreate() {
        super.recreate();
    }
}
